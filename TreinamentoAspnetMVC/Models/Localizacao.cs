﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TreinamentoAspnetMVC.Models
{
    public class Localizacao
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
    }
}