﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TreinamentoAspnetMVC.Models
{
    public class Cliente
    {

        public Cliente(string cpf, string nome, string email)
        {
            this.Cpf = cpf;
            this.Nome = nome;
            this.Email = email;
            Id = Guid.NewGuid();
        }

        public Cliente()
        {
            Id = Guid.NewGuid();
        }


        public void ValidarCliente()
        {
            Erros = new List<string>();
            ValidarCpf();
            //demais validações
        }

        public bool EhValido { get; private set; } = true;

        public List<string> Erros { get; private set; }

        public Guid Id { get; set; }

        public string Cpf { get; set; }

        public DateTime DataNascimento { get; set; }

        [Required]
        public string Nome { get; set; }

        public string Email { get; set; }

        public Guid UsuarioId { get; set; }

        public virtual Usuario Usuario { get; set; }


        public void ValidarCpf()
        {
            if (Cpf.Length != 11)
            {
                Erros.Add("cpf inválido");
                EhValido = false;
            }

           
        }


    }
}