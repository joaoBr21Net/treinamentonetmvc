﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace TreinamentoAspnetMVC.Models.Mapping
{
    public class FuncaoMap : EntityTypeConfiguration<Funcao>
    {
        public FuncaoMap()
        {
            ToTable("funcao");
            HasKey(x => x.Id);
            Property(x => x.FuncaoNome);
        }
    }
}