﻿
using System.Data.Entity.ModelConfiguration;


namespace TreinamentoAspnetMVC.Models.Mapping
{
    public class ClienteMap : EntityTypeConfiguration<Cliente>
    {
        public ClienteMap()
        {
            ToTable("Cliente");
            HasKey(c => c.Id);

            HasRequired(c => c.Usuario);

            Property(p => p.Nome);
            Property(p => p.Cpf);
            Property(p => p.DataNascimento);
            Property(p => p.Email);
      
        }
    }
}