﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace TreinamentoAspnetMVC.Models.Mapping
{
    public class LocalizacaoMap : EntityTypeConfiguration<Localizacao>
    {
        public LocalizacaoMap()
        {

            ToTable("localizacao");
            HasKey(x => x.Id);
            Property(x => x.Descricao);
        }
    }
}