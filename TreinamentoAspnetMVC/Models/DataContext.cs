﻿
using System.Data.Entity;
using TreinamentoAspnetMVC.Models.Mapping;

namespace TreinamentoAspnetMVC.Models
{
    public class DataContext : DbContext
    {

        public DataContext() : base("DataContext")
        {

        }

        public DbSet<Cliente> Clientes { get;  set; }
        public DbSet<Usuario> Usuarios { get;   set; }
        public DbSet<Localizacao> Localizacao { get; set; }
        public DbSet<Funcao> Funcao { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<DataContext>(null);

            modelBuilder.Configurations.Add(new FuncaoMap());
            modelBuilder.Configurations.Add(new LocalizacaoMap());
            modelBuilder.Configurations.Add(new ClienteMap());
            modelBuilder.Configurations.Add(new UsuarioMap());

            base.OnModelCreating(modelBuilder);
        }

    }


   
}