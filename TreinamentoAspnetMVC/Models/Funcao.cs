﻿

namespace TreinamentoAspnetMVC.Models
{
    public class Funcao
    {
        public int Id { get; set; }
        public string FuncaoNome { get; set; }
    }
}