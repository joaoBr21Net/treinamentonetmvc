﻿
using System;

namespace TreinamentoAspnetMVC.Models
{
    public class Usuario
    {

        public Usuario(string login, string senha)
        {
            Login = login;
            Senha = senha;
            Id = Guid.NewGuid();
        }

        public Usuario()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        public string Login { get; set; }

        public string Senha { get; set; }

    }
}